# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Jens Lody
# This file is distributed under the same license as the gnome-shell-extension-openweather package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-shell-extension-openweather 1.1\n"
"Report-Msgid-Bugs-To: https://gitlab.com/jenslody/gnome-shell-extension-"
"openweather/issues\n"
"POT-Creation-Date: 2019-09-16 14:54+0800\n"
"PO-Revision-Date: 2017-09-08 20:50+0300\n"
"Last-Translator: Jiri Grönroos <jiri.gronroos+l10n@iki.fi>\n"
"Language-Team: \n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 1.8.7.1\n"

#: src/extension.js:176
msgid "..."
msgstr "..."

#: src/extension.js:375
msgid ""
"Openweathermap.org does not work without an api-key.\n"
"Either set the switch to use the extensions default key in the preferences "
"dialog to on or register at https://openweathermap.org/appid and paste your "
"personal key into the preferences dialog."
msgstr ""

#: src/extension.js:427
msgid ""
"Dark Sky does not work without an api-key.\n"
"Please register at https://darksky.net/dev/register and paste your personal "
"key into the preferences dialog."
msgstr ""

#: src/extension.js:508
#, javascript-format
msgid "Can not connect to %s"
msgstr "Yhteys kohteeseen %s ei onnistu"

#: src/extension.js:844 data/weather-settings.ui:454
msgid "Locations"
msgstr "Sijainnit"

#: src/extension.js:856
msgid "Reload Weather Information"
msgstr "Lataa säätiedot uudelleen"

#: src/extension.js:871
msgid "Weather data provided by:"
msgstr "Säätiedot tarjoaa:"

#: src/extension.js:883
#, javascript-format
msgid "Can not open %s"
msgstr "Ei voida avata %s"

#: src/extension.js:890
msgid "Weather Settings"
msgstr "Sääasetukset"

#: src/extension.js:941 src/prefs.js:1061
msgid "Invalid city"
msgstr "Virheellinen kaupunki"

#: src/extension.js:952
msgid "Invalid location! Please try to recreate it."
msgstr "Virheellinen sijainti! Luo sijainti uudelleen."

#: src/extension.js:997 data/weather-settings.ui:780
msgid "°F"
msgstr "°F"

#: src/extension.js:999 data/weather-settings.ui:781
msgid "K"
msgstr "K"

#: src/extension.js:1001 data/weather-settings.ui:782
msgid "°Ra"
msgstr "°Ra"

#: src/extension.js:1003 data/weather-settings.ui:783
msgid "°Ré"
msgstr "°Ré"

#: src/extension.js:1005 data/weather-settings.ui:784
msgid "°Rø"
msgstr "°Rø"

#: src/extension.js:1007 data/weather-settings.ui:785
msgid "°De"
msgstr "°De"

#: src/extension.js:1009 data/weather-settings.ui:786
msgid "°N"
msgstr "°N"

#: src/extension.js:1011 data/weather-settings.ui:779
msgid "°C"
msgstr "°C"

#: src/extension.js:1052
msgid "Calm"
msgstr "Tyyntä"

#: src/extension.js:1055
msgid "Light air"
msgstr "Hiljainen tuuli"

#: src/extension.js:1058
msgid "Light breeze"
msgstr "Heikko tuuli"

#: src/extension.js:1061
msgid "Gentle breeze"
msgstr "Heikonlainen tuuli"

#: src/extension.js:1064
msgid "Moderate breeze"
msgstr "Kohtalainen tuuli"

#: src/extension.js:1067
msgid "Fresh breeze"
msgstr "Navakka tuuli"

#: src/extension.js:1070
msgid "Strong breeze"
msgstr "Kova tuuli"

#: src/extension.js:1073
msgid "Moderate gale"
msgstr "Luja tuuli"

#: src/extension.js:1076
msgid "Fresh gale"
msgstr "Myrskyinen tuuli"

#: src/extension.js:1079
msgid "Strong gale"
msgstr "Myrsky"

#: src/extension.js:1082
msgid "Storm"
msgstr "Kova myrsky"

#: src/extension.js:1085
msgid "Violent storm"
msgstr "Ankara myrsky"

#: src/extension.js:1088
msgid "Hurricane"
msgstr "Hirmumyrsky"

#: src/extension.js:1092
msgid "Sunday"
msgstr "Sunnuntai"

#: src/extension.js:1092
msgid "Monday"
msgstr "Maanantai"

#: src/extension.js:1092
msgid "Tuesday"
msgstr "Tiistai"

#: src/extension.js:1092
msgid "Wednesday"
msgstr "Keskiviikko"

#: src/extension.js:1092
msgid "Thursday"
msgstr "Torstai"

#: src/extension.js:1092
msgid "Friday"
msgstr "Perjantai"

#: src/extension.js:1092
msgid "Saturday"
msgstr "Lauantai"

#: src/extension.js:1098
msgid "N"
msgstr "N"

#: src/extension.js:1098
msgid "NE"
msgstr "NE"

#: src/extension.js:1098
msgid "E"
msgstr "E"

#: src/extension.js:1098
msgid "SE"
msgstr "SE"

#: src/extension.js:1098
msgid "S"
msgstr "S"

#: src/extension.js:1098
msgid "SW"
msgstr "SW"

#: src/extension.js:1098
msgid "W"
msgstr "W"

#: src/extension.js:1098
msgid "NW"
msgstr "NW"

#: src/extension.js:1180 src/extension.js:1189 data/weather-settings.ui:817
msgid "hPa"
msgstr "hPa"

#: src/extension.js:1184 data/weather-settings.ui:818
msgid "inHg"
msgstr "inHg"

#: src/extension.js:1194 data/weather-settings.ui:819
msgid "bar"
msgstr "bar"

#: src/extension.js:1199 data/weather-settings.ui:820
msgid "Pa"
msgstr "Pa"

#: src/extension.js:1204 data/weather-settings.ui:821
msgid "kPa"
msgstr "kPa"

#: src/extension.js:1209 data/weather-settings.ui:822
msgid "atm"
msgstr "atm"

#: src/extension.js:1214 data/weather-settings.ui:823
msgid "at"
msgstr "at"

#: src/extension.js:1219 data/weather-settings.ui:824
msgid "Torr"
msgstr "torria"

#: src/extension.js:1224 data/weather-settings.ui:825
msgid "psi"
msgstr "psi"

#: src/extension.js:1229 data/weather-settings.ui:826
msgid "mmHg"
msgstr "mmHg"

#: src/extension.js:1234 data/weather-settings.ui:827
msgid "mbar"
msgstr "mbar"

#: src/extension.js:1278 data/weather-settings.ui:801
msgid "m/s"
msgstr "m/s"

#: src/extension.js:1282 data/weather-settings.ui:800
msgid "mph"
msgstr "mph"

#: src/extension.js:1287 data/weather-settings.ui:799
msgid "km/h"
msgstr "km/h"

#: src/extension.js:1296 data/weather-settings.ui:802
msgid "kn"
msgstr "kn"

#: src/extension.js:1301 data/weather-settings.ui:803
msgid "ft/s"
msgstr "ft/s"

#: src/extension.js:1395
msgid "Loading ..."
msgstr "Ladataan..."

#: src/extension.js:1399
msgid "Please wait"
msgstr "Odota hetki"

#: src/extension.js:1460
msgid "Cloudiness:"
msgstr "Pilvisyys:"

#: src/extension.js:1464
msgid "Humidity:"
msgstr "Kosteus:"

#: src/extension.js:1468
msgid "Pressure:"
msgstr "Paine:"

#: src/extension.js:1472
msgid "Wind:"
msgstr "Tuuli:"

#: src/darksky_net.js:159 src/darksky_net.js:291 src/openweathermap_org.js:352
#: src/openweathermap_org.js:475
msgid "Yesterday"
msgstr "Eilen"

#: src/darksky_net.js:162 src/darksky_net.js:294 src/openweathermap_org.js:354
#: src/openweathermap_org.js:477
#, javascript-format
msgid "%d day ago"
msgid_plural "%d days ago"
msgstr[0] "%d päivä sitten"
msgstr[1] "%d päivää sitten"

#: src/darksky_net.js:174 src/darksky_net.js:176 src/openweathermap_org.js:368
#: src/openweathermap_org.js:370
msgid ", "
msgstr ", "

#: src/darksky_net.js:271 src/openweathermap_org.js:469
msgid "Today"
msgstr "Tänään"

#: src/darksky_net.js:287 src/openweathermap_org.js:471
msgid "Tomorrow"
msgstr "Huomenna"

#: src/darksky_net.js:289 src/openweathermap_org.js:473
#, javascript-format
msgid "In %d day"
msgid_plural "In %d days"
msgstr[0] "%d päivän päästä"
msgstr[1] "%d päivän päästä"

#: src/openweathermap_org.js:183
msgid "Thunderstorm with light rain"
msgstr "Ukkosmyrsky ja kevyttä sadetta"

#: src/openweathermap_org.js:185
msgid "Thunderstorm with rain"
msgstr "Ukkosmyrsky ja sadetta"

#: src/openweathermap_org.js:187
msgid "Thunderstorm with heavy rain"
msgstr "Ukkosmyrsky ja rankkasadetta"

#: src/openweathermap_org.js:189
msgid "Light thunderstorm"
msgstr "Kevyt ukkosmyrsky"

#: src/openweathermap_org.js:191
msgid "Thunderstorm"
msgstr "Ukkosmyrsky"

#: src/openweathermap_org.js:193
msgid "Heavy thunderstorm"
msgstr "Voimakas ukkosmyrsky"

#: src/openweathermap_org.js:195
msgid "Ragged thunderstorm"
msgstr "Satunnaisia ukkosmyrskyjä"

#: src/openweathermap_org.js:197
msgid "Thunderstorm with light drizzle"
msgstr "Ukkosmyrsky ja kevyttä tihkusadetta"

#: src/openweathermap_org.js:199
msgid "Thunderstorm with drizzle"
msgstr "Ukkosmyrsky ja tihkusadetta"

#: src/openweathermap_org.js:201
msgid "Thunderstorm with heavy drizzle"
msgstr "Ukkosmyrsky ja voimakasta tihkusadetta"

#: src/openweathermap_org.js:203
msgid "Light intensity drizzle"
msgstr "Kevyttä tihkusadetta"

#: src/openweathermap_org.js:205
msgid "Drizzle"
msgstr "Tihkusadetta"

#: src/openweathermap_org.js:207
msgid "Heavy intensity drizzle"
msgstr "Voimakasta tihkusadetta"

#: src/openweathermap_org.js:209
msgid "Light intensity drizzle rain"
msgstr "Kevyttä tihkusadetta"

#: src/openweathermap_org.js:211
msgid "Drizzle rain"
msgstr "Tihkusadetta"

#: src/openweathermap_org.js:213
msgid "Heavy intensity drizzle rain"
msgstr "Voimakasta tihkusadetta"

#: src/openweathermap_org.js:215
msgid "Shower rain and drizzle"
msgstr "Sadekuuroja ja tihkusadetta"

#: src/openweathermap_org.js:217
msgid "Heavy shower rain and drizzle"
msgstr "Voimakkaita sadekuuroja ja tihkusadetta"

#: src/openweathermap_org.js:219
msgid "Shower drizzle"
msgstr "Tihkusadekuuroja"

#: src/openweathermap_org.js:221
msgid "Light rain"
msgstr "Kevyttä sadetta"

#: src/openweathermap_org.js:223
msgid "Moderate rain"
msgstr "Kohtalaista sadetta"

#: src/openweathermap_org.js:225
msgid "Heavy intensity rain"
msgstr "Voimakasta sadetta"

#: src/openweathermap_org.js:227
msgid "Very heavy rain"
msgstr "Erittäin voimakasta sadetta"

#: src/openweathermap_org.js:229
msgid "Extreme rain"
msgstr "Äärimmäisen voimakasta sadetta"

#: src/openweathermap_org.js:231
msgid "Freezing rain"
msgstr "Jäätävää sadetta"

#: src/openweathermap_org.js:233
msgid "Light intensity shower rain"
msgstr "Kevyitä sadekuuroja"

#: src/openweathermap_org.js:235
msgid "Shower rain"
msgstr "Sadekuuroja"

#: src/openweathermap_org.js:237
msgid "Heavy intensity shower rain"
msgstr "Voimakkaita sadekuuroja"

#: src/openweathermap_org.js:239
msgid "Ragged shower rain"
msgstr "Satunnaisia sadekuuroja"

#: src/openweathermap_org.js:241
msgid "Light snow"
msgstr "Kevyttä lumisadetta"

#: src/openweathermap_org.js:243
msgid "Snow"
msgstr "Lumisadetta"

#: src/openweathermap_org.js:245
msgid "Heavy snow"
msgstr "Voimakasta lumisadetta"

#: src/openweathermap_org.js:247
msgid "Sleet"
msgstr "Räntäsadetta"

#: src/openweathermap_org.js:249
msgid "Shower sleet"
msgstr "Räntäsadekuuroja"

#: src/openweathermap_org.js:251
msgid "Light rain and snow"
msgstr "Kevyttä sadetta ja lunta"

#: src/openweathermap_org.js:253
msgid "Rain and snow"
msgstr "Sadetta ja lunta"

#: src/openweathermap_org.js:255
msgid "Light shower snow"
msgstr "Kevyitä lumikuuroja"

#: src/openweathermap_org.js:257
msgid "Shower snow"
msgstr "Lumikuuroja"

#: src/openweathermap_org.js:259
msgid "Heavy shower snow"
msgstr "Voimakkaita lumikuuroja"

#: src/openweathermap_org.js:261
msgid "Mist"
msgstr "Usvaa"

#: src/openweathermap_org.js:263
msgid "Smoke"
msgstr "Savua"

#: src/openweathermap_org.js:265
msgid "Haze"
msgstr "Usvaa"

#: src/openweathermap_org.js:267
msgid "Sand/Dust Whirls"
msgstr "Hiekka- ja pölypyörteitä"

#: src/openweathermap_org.js:269
msgid "Fog"
msgstr "Sumua"

#: src/openweathermap_org.js:271
msgid "Sand"
msgstr "Hiekkaa"

#: src/openweathermap_org.js:273
msgid "Dust"
msgstr "Pölyistä"

#: src/openweathermap_org.js:275
msgid "VOLCANIC ASH"
msgstr "TULIVUORIPÖLYÄ"

#: src/openweathermap_org.js:277
msgid "SQUALLS"
msgstr "UKKOSPUUSKIA"

#: src/openweathermap_org.js:279
msgid "TORNADO"
msgstr "TORNADO"

#: src/openweathermap_org.js:281
msgid "Sky is clear"
msgstr "Taivas on selkeä"

#: src/openweathermap_org.js:283
msgid "Few clouds"
msgstr "Muutamia pilviä"

#: src/openweathermap_org.js:285
msgid "Scattered clouds"
msgstr "Ajoittain pilvistä"

#: src/openweathermap_org.js:287
msgid "Broken clouds"
msgstr "Melkein pilvistä"

#: src/openweathermap_org.js:289
msgid "Overcast clouds"
msgstr "Pilvistä"

#: src/openweathermap_org.js:291
msgid "Not available"
msgstr "Ei saatavilla"

#: src/prefs.js:193 src/prefs.js:247 src/prefs.js:294 src/prefs.js:301
#, javascript-format
msgid "Invalid data when searching for \"%s\""
msgstr "Virheelliset tiedot etsiessä \"%s\""

#: src/prefs.js:201 src/prefs.js:263 src/prefs.js:307
#, javascript-format
msgid "\"%s\" not found"
msgstr "Ei löydetty \"%s\""

#: src/prefs.js:367
msgid "Location"
msgstr "Sijainti"

#: src/prefs.js:378
msgid "Provider"
msgstr "Tarjoaja"

#: src/prefs.js:551
#, javascript-format
msgid "Remove %s ?"
msgstr "Poistetaanko %s ?"

#: src/prefs.js:1096
msgid "default"
msgstr "oletus"

#: data/weather-settings.ui:31
msgid "Edit name"
msgstr "Muokkaa nimeä"

#: data/weather-settings.ui:49 data/weather-settings.ui:79
#: data/weather-settings.ui:231
msgid "Clear entry"
msgstr "Tyhjennä tietue"

#: data/weather-settings.ui:63
msgid "Edit coordinates"
msgstr "Muokkaa koordinaatteja"

#: data/weather-settings.ui:93 data/weather-settings.ui:268
msgid "Extensions default weather provider"
msgstr "Laajennusten säätietojen oletustarjoaja"

#: data/weather-settings.ui:125 data/weather-settings.ui:300
msgid "Cancel"
msgstr "Peru"

#: data/weather-settings.ui:143 data/weather-settings.ui:318
msgid "Save"
msgstr "Tallenna"

#: data/weather-settings.ui:208
msgid "Search by location or coordinates"
msgstr "Etsi sijaintia nimellä tai koordinaateilla"

#: data/weather-settings.ui:232
msgid "e.g. Vaiaku, Tuvalu or -8.5211767,179.1976747"
msgstr "esim. Vaiaku, Tuvalu tai -8.5211767,179.1976747"

#: data/weather-settings.ui:242
msgid "Find"
msgstr "Etsi"

#: data/weather-settings.ui:475
msgid "Chose default weather provider"
msgstr "Säätietojen oletustarjoaja"

#: data/weather-settings.ui:488
msgid "Personal Api key from openweathermap.org"
msgstr "Henkilökohtainen API-avain openweathermap.orgista"

#: data/weather-settings.ui:539
msgid "Personal Api key from Dark Sky"
msgstr "Henkilökohtainen API-avain Dark Skysta"

#: data/weather-settings.ui:552
msgid "Refresh timeout for current weather [min]"
msgstr "Päivityksen aikakatkaisu nykyiselle säätiedoille [min]"

#: data/weather-settings.ui:566
msgid "Refresh timeout for weather forecast [min]"
msgstr "Päivityksen aikakatkaisu sääennusteelle [min]"

#: data/weather-settings.ui:594
msgid ""
"Note: the forecast-timout is not used for Dark Sky, because they do not "
"provide seperate downloads for current weather and forecasts."
msgstr ""

#: data/weather-settings.ui:622
msgid "Use extensions api-key for openweathermap.org"
msgstr ""

#: data/weather-settings.ui:633
msgid ""
"Switch off, if you have your own api-key for openweathermap.org and put it "
"into the text-box below."
msgstr ""

#: data/weather-settings.ui:650
msgid "Weather provider"
msgstr "Säätietojen tarjoaja"

#: data/weather-settings.ui:670
msgid "Chose geolocation provider"
msgstr "Valitse sijainnin tarjoaja"

#: data/weather-settings.ui:696
msgid "Personal AppKey from developer.mapquest.com"
msgstr "Henkilökohtainen AppKey developer.mapquest.comista"

#: data/weather-settings.ui:725
msgid "Geolocation provider"
msgstr "Sijainnin tarjoaja"

#: data/weather-settings.ui:745
#: data/org.gnome.shell.extensions.openweather.gschema.xml:63
msgid "Temperature Unit"
msgstr "Lämpötilan yksikkö"

#: data/weather-settings.ui:756
msgid "Wind Speed Unit"
msgstr "Tuulen nopeuden yksikkö"

#: data/weather-settings.ui:767
#: data/org.gnome.shell.extensions.openweather.gschema.xml:67
msgid "Pressure Unit"
msgstr "Paineen yksikkö"

#: data/weather-settings.ui:804
msgid "Beaufort"
msgstr "bofori"

#: data/weather-settings.ui:844
msgid "Units"
msgstr "Yksiköt"

#: data/weather-settings.ui:864
#: data/org.gnome.shell.extensions.openweather.gschema.xml:113
msgid "Position in Panel"
msgstr "Sijainti paneelissa"

#: data/weather-settings.ui:875
msgid "Position of menu-box [%] from 0 (left) to 100 (right)"
msgstr "Sijainti valikkolaatikolle [%] arvosta 0 (vasen) arvoon 100 (oikea)"

#: data/weather-settings.ui:886
#: data/org.gnome.shell.extensions.openweather.gschema.xml:76
msgid "Wind Direction by Arrows"
msgstr "Tuulen suunta nuolina"

#: data/weather-settings.ui:897
#: data/org.gnome.shell.extensions.openweather.gschema.xml:89
msgid "Translate Conditions"
msgstr "Käännä olosuhteet"

#: data/weather-settings.ui:908
#: data/org.gnome.shell.extensions.openweather.gschema.xml:93
msgid "Symbolic Icons"
msgstr "Symboliset kuvakkeet"

#: data/weather-settings.ui:919
msgid "Text on buttons"
msgstr "Teksti painikkeissa"

#: data/weather-settings.ui:930
#: data/org.gnome.shell.extensions.openweather.gschema.xml:101
msgid "Temperature in Panel"
msgstr "Lämpötila paneelissa"

#: data/weather-settings.ui:941
#: data/org.gnome.shell.extensions.openweather.gschema.xml:105
msgid "Conditions in Panel"
msgstr "Olosuhteet paneelissa"

#: data/weather-settings.ui:952
#: data/org.gnome.shell.extensions.openweather.gschema.xml:109
msgid "Conditions in Forecast"
msgstr "Olosuhteet ennusteessa"

#: data/weather-settings.ui:963
msgid "Center forecast"
msgstr "Keskitä ennuste"

#: data/weather-settings.ui:974
#: data/org.gnome.shell.extensions.openweather.gschema.xml:137
msgid "Number of days in forecast"
msgstr "Päivien määrä ennusteessa"

#: data/weather-settings.ui:985
#: data/org.gnome.shell.extensions.openweather.gschema.xml:141
msgid "Maximal number of digits after the decimal point"
msgstr "Desimaalipilkun jälkeen tulevien numeroiden maksimimäärä"

#: data/weather-settings.ui:997
msgid "Center"
msgstr "Keskellä"

#: data/weather-settings.ui:998
msgid "Right"
msgstr "Oikealla"

#: data/weather-settings.ui:999
msgid "Left"
msgstr "Vasemmalla"

#: data/weather-settings.ui:1154
msgid "Maximal number of characters in location label"
msgstr ""

#: data/weather-settings.ui:1185
msgid "Layout"
msgstr "Asettelu"

#: data/weather-settings.ui:1237
msgid "Version: "
msgstr "Versio: "

#: data/weather-settings.ui:1251
msgid "unknown (self-build ?)"
msgstr "tuntematon (koostettu itse?)"

#: data/weather-settings.ui:1271
msgid ""
"<span>Weather extension to display weather information from <a href="
"\"https://openweathermap.org/\">Openweathermap</a> or <a href=\"https://"
"darksky.net\">Dark Sky</a> for almost all locations in the world.</span>"
msgstr ""
"<span>Laajennus säätietojen näyttämiseksi <a href=\"https://openweathermap."
"org/\">Openweathermapista</a> tai <a href=\"https://darksky.net\">Dark "
"Skysta</a> melkein maailman jokaiseen kolkkaan.</span>"

#: data/weather-settings.ui:1294
msgid "Maintained by"
msgstr "Ylläpitäjä"

#: data/weather-settings.ui:1324
msgid "Webpage"
msgstr "Verkkosivusto"

#: data/weather-settings.ui:1344
msgid ""
"<span size=\"small\">This program comes with ABSOLUTELY NO WARRANTY.\n"
"See the <a href=\"https://www.gnu.org/licenses/old-licenses/gpl-2.0.html"
"\">GNU General Public License, version 2 or later</a> for details.</span>"
msgstr ""
"<span size=\"small\">Tämä ohjelma EI SISÄLLÄ MINKÄÄNLAISTA TAKUUTA.\n"
"Lue lisätietoja <a href=\"https://www.gnu.org/licenses/old-licenses/gpl-2.0."
"html\">GNU General Public -lisenssin versiosta 2 tai myöhemmästä</a>.</span>"

#: data/weather-settings.ui:1365
msgid "About"
msgstr "Tietoja"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:55
msgid "Weather Provider"
msgstr "Säätietojen tarjoaja"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:59
msgid "Geolocation Provider"
msgstr "Sijainnin tarjoaja"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:71
msgid "Wind Speed Units"
msgstr "Tuulen nopeuden yksiköt"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:72
msgid ""
"Choose the units used for wind speed. Allowed values are 'kph', 'mph', 'm/"
"s', 'knots', 'ft/s' or 'Beaufort'."
msgstr ""
"Valitse yksikkö tuulen nopeudelle. Sallitut arvot ovat 'kph', 'mph', 'm/s', "
"'knots', 'ft/s' tai 'bofori'."

#: data/org.gnome.shell.extensions.openweather.gschema.xml:77
msgid "Choose whether to display wind direction through arrows or letters."
msgstr "Valitse näytetäänkö tuulen suunta nuolina vai kirjaimina."

#: data/org.gnome.shell.extensions.openweather.gschema.xml:81
msgid "City to be displayed"
msgstr "Näytettävä kaupunki"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:85
msgid "Actual City"
msgstr "Todellinen kaupunki"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:97
msgid "Use text on buttons in menu"
msgstr "Käytä tekstiä valikon painikkeissa"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:117
msgid "Horizontal position of menu-box."
msgstr "Valikkolaatikon vaakasuuntainen sijainti."

#: data/org.gnome.shell.extensions.openweather.gschema.xml:121
msgid "Refresh interval (actual weather)"
msgstr "Päivitysväli (säätiedot)"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:125
msgid "Maximal length of the location text"
msgstr ""

#: data/org.gnome.shell.extensions.openweather.gschema.xml:129
msgid "Refresh interval (forecast)"
msgstr "Päivitysväli (ennuste)"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:133
msgid "Center forecastbox."
msgstr "Keskitä ennustelaatikko."

#: data/org.gnome.shell.extensions.openweather.gschema.xml:145
msgid "Your personal API key from openweathermap.org"
msgstr "Henkilökohtainen API-avaimesi openweathermap.orgista"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:149
msgid "Use the extensions default API key from openweathermap.org"
msgstr ""

#: data/org.gnome.shell.extensions.openweather.gschema.xml:153
msgid "Your personal API key from Dark Sky"
msgstr "Henkilökohtainen API-avaimesi Dark Skysta"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:157
msgid "Your personal AppKey from developer.mapquest.com"
msgstr "Henkilökohtainen AppKey developer.mapquest.comista"
